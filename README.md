# The Health Manager - THM App

## About

There is bodies of research out there about many topics that will become
relevant for the future ‘front line’ in the post corona crisis management teams
particularly General Practicioners. The restauration and innovation of health
care systems, the preparation for this and future pandemics for other
countries as well as the long overdue innovation of hospital and public
health management asks for a more direct transfer between researchers and
practitioners as well as information transfer to patients directly. There
is a lot of confusion around news and facts surround the topic.

Therefore we would like to create an app, that is based on academic research
and which analyses and tailors the information according to the questions of
three types of groups:
1. general public / patients
2. the healthcare workers and
3. GPs / Surgeons or researchers.

This tool should provide informed answers with regards to the Context,
the Theory, the Data of the underlying research in an easily understandable
format (Direct answers to questions or even short videos reflecting the relevant
content).

---

### Website

Details on how to contribute to the [website](https://gitlab.com/the-health-manager/site/-/blob/master/CONTRIBUTING.md)

Website pipeline status: [![pipeline status](https://gitlab.com/the-health-manager/site/badges/master/pipeline.svg)](https://gitlab.com/the-health-manager/site/-/commits/master)

---

### App

The code that is running in AWS SageMaker to generate the summaries is at the [Presumm THM Repository](https://gitlab.com/the-health-manager/presumm-thm)