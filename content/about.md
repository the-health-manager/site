---
title: About Us
subtitle: >-
  About Us
image: images/about.jpg
layout: page
---

We are a research-spin-off and entrepreneurial team from the StartUp Incubator at the Bauhaus University Weimar.

Daniela Marzavan - https://www.linkedin.com/in/daniela-marzavan-a202638/ - https://www.changedarer.com

Sophie Gruböck - https://www.linkedin.com/in/sophie-gruboeck/

---

Uyen Huynh-do, Associate Professor of Nephrology and Medicine, University of Bern Medical School - https://www.linkedin.com/in/uyen-huynh-do-86111834/

Prabitha Urwyler - Deputy Group leader/ Senior Researcher at ARTORG Center for Biomedical Engineering, Gerontechnology and Rehabilitation - https://www.linkedin.com/in/prabithaurwyler/

Wes Wilson - Oncologist, PhD

---

Jan-Christopher Pien - Software Development Engineer, Amazon - https://www.linkedin.com/in/jessepeng/

Hannah Suarez - Technical Evangelist - https://www.hannahsuarez.me
