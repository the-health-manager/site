---
title: Contact
sections:
  - section_id: contact
    type: section_contact
    background: gray
    title: Contact
    content: To contact us please open a new issue in [The health manager](https://gitlab.com/the-health-manager/) Gitlab Repository
layout: landing
---
