---
title: Home
sections:
  - section_id: hero
    type: section_hero
    image: images/hero2.png
    title: The health manager
    content: |-
      Solving the GP, health innovators and crisis manager dilemma for knowledge transfer during and post corona times.
    actions:
      - label: Learn More
        url: "/features"
  - section_id: features
    type: section_features
    background: gray
    title: Features
    subtitle: >-
      What we are solving
    features:
      - title: Delivering a Solution
        image: images/feature1.png
        content: |-
          There is bodies of research out there about many topics that will become relevant for the future ‘front line’ in the post corona crisis management teams - particularly General Practitioners. The restoration and innovation of health care systems, the preparation for this and future pandemics for other countries as well as the long overdue innovation of hospital and public health management asks for a more direct transfer between researchers and practitioners as well as information transfer to patients directly. There is a lot of confusion around news and facts surround the topic.
        actions:
          - label: Learn More
            url: "/features"
      - title: Leveraging Academic Research
        image: images/feature2.png
        content: |-
           We are creating an app that is based on academic research which analyses and tailors the information according to the questions of a GPs patient or even hospital patients. This should be the Health Manager's Tool to provide informed answers with regards to the Context, the theory, the data that the research is based on and the outcome it provides creating knowledge transfer that is needed in times of crises.
        actions:
          - label: Learn More
            url: "/features"
      - title: Research Paper in your Pocket
        image: images/feature3.png
        content: |-
          Pack 10.000 research paper in the Pocket of future Health Innovators/ Healthmanagers.
        actions:
          - label: Research Papers
            url: "/features"
  - section_id: Research
    type: section_reviews
    background: white
    title: Types of questions to ask?
    subtitle: >-

    reviews:
      - author: Rafael Guerrer MD
        avatar: images/review1.jpg
        content: I am a GP, based in Barcelona. I wonder how our health care providers can be more efficient on providing us with materials we need like protection and with knowledge we need like latest research on CoVid.
      - author: Rachel Bloch MD
        avatar: images/review2.jpg
        content: >-
          I am an ER doctor, based in New York. I need to obtain the latest
          evidence-based information coming from different schools of thought, not just papers that are the most rated.
      - author: Jerome Asher
        avatar: images/review3.jpg
        content: >-
          As part of the general public, I want access to the most reliable
          info there is about CoVid. There is a lot of questionable posts out there on social media and I don't have the time to read the full research papers.
  - section_id: call-to-action
    type: section_cta
    title: View the MVP
    subtitle: >-
      View the first iteration of the MVP
    actions:
      - label: Get Started (Coming Soon)
        url: "/signup"
  - section_id: recent-posts
    type: section_posts
    background: gray
    title: Our Thoughts
layout: landing
---
