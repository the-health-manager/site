---
title: Features
sections:
  - section_id: features
    type: section_content
    background: gray
    image: images/app_flow.png
    title: Features of The Health Manager
    content: |-
      Solving the health innovators and crises manager dilemma for knowledge transfer during and post corona times.

      * Deliver information based on academic research
      * Analyses and tailors the information according to the questions of three types of groups: 1. general public / patients 2. the healthcare workers and 3. GPs / Surgeons or researchers.
      *  A more direct transfer between researchers and practitioners as well as information transfer to patients directly

      this tool should provide informed answers with regards to the context & theory, Methodology & Data Set and Outcomes & Implications of the underlying research in an easily understandable format such as printable graphic summaries and short videos.

    actions:
      - label: GitLab
        url: "https://gitlab.com/the-health-manager/"
      - label: View Demo
        url: "try-it-out"
  - section_id: call-to-action
    type: section_cta
    title: Contribute Today!
    subtitle: >-
      Contribute to our project on GitLab
    actions:
      - label: Contribute
        url: "https://gitlab.com/the-health-manager/"
layout: landing
---
