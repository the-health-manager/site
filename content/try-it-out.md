---
title: Try it out
sections:
  - section_id: features
    type: section_content
    background: gray
#    image: images/feature1.png
    title: Try it out!
    content: |-
      You can try the summarizer functionality here. Enter the content of a research paper and have it summarized!
  - section_id: try_it_out
    type: section_try-it-out
    background: gray
layout: landing
---
    actions:
      - label: Free Trial
        url: "/signup"
      - label: View Demo
        url: "#"
  - section_id: call-to-action
    type: section_cta
    title: Contribute Today!
    subtitle: >-
      Contribute to our project on GitLab
    actions:
      - label: Contribute
        url: "https://gitlab.com/the-health-manager/"
layout: landing
---
