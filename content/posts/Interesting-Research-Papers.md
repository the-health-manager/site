---
title: Interesting articles and resources
excerpt: >-
    In this post, we share articles, papers and resources of note about the project
date: 2020-04-11
thumb_image: images/blog_thumb.jpg
image: images/blog_hero.jpg
layout: post
---

[COVID-19 Open Research Dataset Challenge (CORD-19)](https://www.kaggle.com/allen-institute-for-ai/CORD-19-research-challenge/tasks)

[Transformer-Based Language Model Writes Abstracts For Scientific Papers](https://syncedreview.com/2019/09/23/transformer-based-language-model-writes-abstracts-for-scientific-papers/)
