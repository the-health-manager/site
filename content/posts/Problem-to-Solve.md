---
title: Cognitive Discovery
subtitle: >-

excerpt: >-
     There area of work this project is talking about is called cognitive discovery AI, MIT has been working on it for years and they have made some really good progress in 2018
date: 2020-04-11
thumb_image: images/blog_thumb.jpg
image: images/blog_hero.jpg
layout: post
---

You can read their paper [here](https://www.mitpressjournals.org/doi/full/10.1162/tacl_a_00258) but as you can see there are still some challenges with it, another company that has tried to do something similar is https://iris.ai/ for academia. This is where you can scan the literature and pull out the important parts you need to know. In Australia in there is a doctor named Samuel Gluck, who also has been working on AI to go through specifically medical journals to pull out the important information for diseases for doctors and another version for patients.
