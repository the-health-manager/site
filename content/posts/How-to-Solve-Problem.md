---
title: There is a semantic problem here...
excerpt: >-
    Research is always done in a specific context.
date: 2020-04-11
thumb_image: images/blog_thumb.jpg
image: images/blog_hero.jpg
layout: post
---

Here is some notes from a call with Bart de Witte who is one of the leading experts for digital transformation in healthcare and one of the most influential forward thinkers of the digital health era. He is a medical AI activist that believes medical knowledge needs to stay open. He founded the HIPPO AI Foundation, the first global NGO for open source-based artificial intelligence in medicine, and the Digital Health Academy in Berlin. He is also part of the founding faculty of the European Institute of Exponential Technologies and Desirable Futures, futur.io. An institute that focuses on finding alternative European strategies for today's postmodernity and developing new methods to open the barriers between different possibilities and preferences for a more ideal future with greater social benefits. He worked for IBM for eight years - most recently as Director Digital Health at IBM - and previously for SAP as Business Development Director.


He pointed us to this [Youtube channel(https://www.youtube.com/user/keeroyz).

Research is always done in a specific context. we need machine deep understanding to differentiate between Understanding what the CONTEXT is! and what the paper says (which we mostly find in the abstract). So this sports the assumptions that we should divide up the paper in 1. context, 2, methodology 3. outcome to shed light on the context of this outcome for the general audience to understand how to process that outcome.

He also pointed out we are looking for natural language generation not processing — there is no standard technology there.

So question is: is it feasible to create an own experience manually and then train the algorithm?
