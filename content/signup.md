---
title: Contribute
subtitle: >-
  How to Contribute to THM
image: images/about.jpg
layout: page
---

_We are looking for contributors in the following areas:_

* Natural Language Processing

* Machine Learning

* Data Science

* Open Source Development

_Our Stack:_

* Jekyll (Static Site Generator)

* Gitlab Pages/Gitlab

* Unibit/NPM

* Balsamiq (Wireframing)

* Docker

* AWS S3

* AWS SageMaker

* Python

* Open Source Neural Machine Translation in PyTorch (OpenNMT-py)

* Stanford CoreNLP

Please go to the Gitlab project page https://gitlab.com/the-health-manager/
