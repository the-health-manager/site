---
title: Research
sections:
  - section_id: features
    type: section_content
    background: gray
#    image: images/feature1.png
    title: What research papers are available in The health manager
    content: |-
      Solving the health innovators and crises manager dilemma for knowledge transfer during and post corona times.
  - section_id: faq
    type: section_faq
    background: gray
    title: What types of questions can be typically asked?
    subtitle: >-
         Below is a list of typical questions that can be asked, compiled by our health educators.
    faq_items:
      - question: Is Coronavirus airborne?
#        answer: |-
#          Ut cursus, nunc vitae hendrerit viverra, massa ipsum congue quam, sed tempus mauris lacus sit amet nibh. Curabitur laoreet est maximus mollis feugiat. Praesent nibh libero, placerat et justo at, luctus tristique enim. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
      - question: Does everybody need to wear a mask?
      - question: Can my pet get Covid19?
      - question: Are young people safe?
      - question: Are men more endangered than women?
      - question: What about pregnancy during Covid19?
      - question: How good are serological tests?
      - question: Can you get Covid19 twice?
      - question: Should I take hydroxycholoroquine?
      - question: Is there going to be a vaccine?
      - question: Will coronavirus survive airborne?
      - question: Are young people safe?
      - question: Do face masks protect me?
      - question: Are men more likely to die?
      - question: How long can it live in your bathroom?
      - question: Does washing clothes protect against the disease?
layout: landing
---
    actions:
      - label: Free Trial
        url: "/signup"
      - label: View Demo
        url: "#"
  - section_id: call-to-action
    type: section_cta
    title: Contribute Today!
    subtitle: >-
      Contribute to our project on GitLab
    actions:
      - label: Contribute
        url: "https://gitlab.com/the-health-manager/"
layout: landing
---
