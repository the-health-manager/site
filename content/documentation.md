---
title: Documentation
sections:
  - section_id: features
    type: section_content
    background: gray
#    image: images/feature1.png
    title: Documentation about the health manager
    content: |-
      There are **three** 'modes' of 'consuming' research papers.

      They are based on a model of social practice theory that basically says we need these three channels to embody and really understand information and apply it to our everyday routines.

      The idea behind this is to have each research paper or body of research papers that fit the users' question' (something they fill in upfront, for example, _"I am a GP, based in Barcelona, I wonder how our health care provider can be more efficient on providing us with materials we need like protection. And with knowledge we need like latest research on CoVid."_).

      The algorithm then scans through the data base and finds that 3-5 research papers answer the GPs question on a scale form 1-10. Let's say there's a paper providing full literature review on Corona Viruses in general. There is another paper on Health care management supply. And one from informatics on algorithms of distributing scarce goods

    actions:
      - label: Contribute
        url: "https://gitlab.com/the-health-manager/"
      - label: View Demo
        url: "#"
  - section_id: call-to-action
    type: section_cta
    title: Contribute Today!
    subtitle: >-
      Contribute to our project on GitLab
    actions:
      - label: Contribute
        url: "https://gitlab.com/the-health-manager/site/-/blob/master/CONTRIBUTING.md"
layout: landing
---
