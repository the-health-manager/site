// Responsive video embeds
var videoEmbeds = [
  'iframe[src*="youtube.com"]',
  'iframe[src*="vimeo.com"]'
];
reframe(videoEmbeds.join(','));

// Mobile menu
var menuToggle = document.querySelectorAll('.menu-toggle');

for (var i = 0; i < menuToggle.length; i++) {
  menuToggle[i].addEventListener('click', function(e){
    document.body.classList.toggle('menu--opened');
    e.preventDefault();
  },false);
}

document.body.classList.remove('menu--opened');

window.addEventListener('resize', function () {
  if (menuToggle.offsetParent === null) {
    document.body.classList.remove('menu--opened');
  }
}, true);

// Accordion
var accordions = document.querySelectorAll('.faq-accordion');
Array.from(accordions).forEach((accordion) => {
  var ba = new BadgerAccordion(accordion, {
    headerClass: '.accordion-trigger',
    panelClass: '.accordion-panel',
    panelInnerClass: '.accordion-content',
    openMultiplePanels: true
  });
});

const apiUrl = 'https://6la890ol7b.execute-api.eu-central-1.amazonaws.com/prod/create-summary'

function status(response) {
  if (response.status >= 200 && response.status < 300) {
    return Promise.resolve(response)
  } else {
    return Promise.reject(new Error(response.statusText))
  }
}

function json(response) {
  return response.json()
}

// Try it out
const summarizeText = () => {
  const textArea = document.querySelector("form.try-it-out-form textarea")
  const resultParagraph = document.querySelector("p#try-it-out-result")
  const loadingSpinner = document.querySelector(".loader")
  const summarizeButton = document.querySelector("button#try-it-out")
  const requestObject = {
    text: textArea.value
  }
  loadingSpinner.style.display = "inline-block"
  summarizeButton.disabled = true
  fetch(apiUrl, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json'},
    body: JSON.stringify(requestObject)
  }).then(status)
    .then(json)
    .then(result => resultParagraph.innerHTML = result["summary"])
    .catch(error => console.log('Request failed', error))
    .finally(() => {
      loadingSpinner.style.display = "none"
      summarizeButton.disabled = false
    })
}