# Contributing to The Health Manager Website

## Website

This website was forked for the #EasterHack hackathon.

As is the case with hackathons were there is only a short time to create a project and the focus is more on prototyping an idea
rather than spending time on branding, color palettes, etc this website's template is largely based on this template and associated designs.

**How to run the website locally:**

The website is forked from the [Jekyll "Azimuth" Stackbit theme](https://github.com/stackbithq/stackbit-theme-azimuth) authored originally for the
Stackbit CMS. 

1. Run `git clone` for the repository `git clone git@gitlab.com:the-health-manager/site.git` This will download all files to your local
   computer into a folder `site`.

2. Navigate to the `site` folder.

3. Develop locally using the Unibit CLI. Run `npm install -g @stackbit/unibit` This will install the required development environment.
   You may need to have `npm` first installed if you don't have this.

3. To start the local development server, run `unibit develop`.

4. It will then run locally and accessible on a web browser on `http://localhost:5000/`. The port number may not be `5000` and it may change.

5. To compile a production build into the `public` folder, run `unibit build`

Notes:

* In `config.yml` the `baseurl:` must be `"/"` to properly show the site when running in `localhost`.


## App

See the [PreSumm-THM repository](https://gitlab.com/the-health-manager/presumm-thm)
